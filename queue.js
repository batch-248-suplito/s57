let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}

function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  for (let i = 0; i < collection.length - 1; i++) {
    collection[i] = collection[i + 1];
  }

  collection.length = collection.length - 1;
  return collection;
}

function front() {
  return collection[0];
}

function size() {
  let count = 0;
  for (let i = 0; i < collection.length; i++) {
    count++;
  }
  return count;
}

function isEmpty() {
  if (collection[0] === undefined) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
